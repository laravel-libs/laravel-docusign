<?php

namespace Pilyavskiy\Docusign;

use Illuminate\Support\Facades\Facade;

/**
 * Class DocusignFacade
 * @package Pilyavskiy\Docusign
 *
 * @see \Pilyavskiy\Docusign\Managers\DocusignManager
 *
 * @see \Pilyavskiy\Docusign\Managers\UserTrait
 * @method static getUsers()
 * @method static getUser($userId, bool $additionalInfo = false)
 *
 * @see \Pilyavskiy\Docusign\Managers\TemplateTrait
 * @method static getTemplates($options = null)
 * @method static getTemplate($templateId)
 * @method static getEnvelopeTemplates($envelopeId)
 *
 * @see \Pilyavskiy\Docusign\Managers\EnvelopeTrait
 * @method static getEnvelopes(array $envelopeIds)
 * @method static getEnvelope($envelopeId)
 * @method static getEnvelopePdf($envelopeId)
 * @method static getEnvelopeRecipients($envelopeId, $include_tabs = false)
 * @method static getEnvelopeTabs($envelopeId, $recipientId)
 * @method static getEnvelopeCustomFields($envelopeId)
 *
 * @method static createEnvelope($data)
 * @method static createEnvelopeRecipientView($envelopeId, $data)
 *
 * @method static updateEnvelope($envelopeId, $data)
 * @method static updateEnvelopeRecipients($envelopeId, $recipients)
 * @method static updateRecipientTabs($envelopeId, $recipientId, $tabs)
 *
 * @method static deleteEnvelopes(array $envelopeIds)
 *
 * @see \Pilyavskiy\Docusign\Managers\FolderTrait
 * @method static getFolders($withInclude = false)
 * @method static getFolderEnvelopes($folderId, $options = null)
 */
class DocusignFacade extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'docusign';
    }
}
