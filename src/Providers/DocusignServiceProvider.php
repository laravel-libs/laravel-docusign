<?php

namespace Pilyavskiy\Docusign\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\AliasLoader;
use Pilyavskiy\Docusign\Managers\DocusignManager;

class DocusignServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->alias('docusign', DocusignManager::class);

        $this->app->singleton(
            'docusign',
            function ($app) {
                return new DocusignManager($app->config->get('docusign', []));
            }
        );
    }

    public function boot()
    {
        $this->publishes([
            __DIR__.'/../config/docusign.php' => config_path('docusign.php'),
        ]);

        $this->mergeConfigFrom(
            realpath(__DIR__.'/../config/docusign.php'), 'docusign'
        );
    }

    public function provides()
    {
        return ['docusign', DocusignManager::class];
    }

}
