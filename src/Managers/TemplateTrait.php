<?php

namespace Pilyavskiy\Docusign\Managers;

trait TemplateTrait
{
    public function getTemplates($options = null)
    {
        $response = $this->client->get('templates', ['query' => $options]);

        return json_decode($response->getBody()->getContents(), true)['envelopeTemplates'] ?? [];
    }

    public function getTemplate($templateId)
    {
        $response = $this->client->get('templates/' . $templateId);

        return json_decode($response->getBody()->getContents(), true);
    }

    public function getEnvelopeTemplates($envelopeId)
    {
        $response = $this->client->get('envelopes/' . $envelopeId . '/templates');

        return json_decode($response->getBody()->getContents(), true)['templates'] ?? [];
    }
}
