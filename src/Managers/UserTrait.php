<?php

namespace Pilyavskiy\Docusign\Managers;

trait UserTrait
{
    public function getUsers()
    {
        $response = $this->client->get('users');

        return json_decode($response->getBody()->getContents(), true)['users'] ?? [];
    }

    public function getUser($userId, bool $additionalInfo = false)
    {
        $response = $this->client->get('users/' . $userId . '?additional_info=' . ($additionalInfo) ? 'true' : 'false');

        return json_decode($response->getBody()->getContents(), true);
    }
}
