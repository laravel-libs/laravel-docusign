<?php

namespace Pilyavskiy\Docusign\Managers;

use GuzzleHttp\Client;

class DocusignManager
{
    use EnvelopeTrait, FolderTrait, TemplateTrait, UserTrait;

    private $client;

    public function __construct(array $config = [], array $clientSettings = [])
    {
        foreach ($config as $key => $item) {
            if (empty($item)) {
                throw new \InvalidArgumentException('Empty value in config, please fill in all configuration fields!');
            }
        }

        if(array_key_exists('query', $clientSettings)) {
            unset($clientSettings['query']);
        }
        if(array_key_exists('json', $clientSettings)) {
            unset($clientSettings['json']);
        }

        $headers = [
            'X-DocuSign-Authentication' => '<DocuSignCredentials><Username>' . $config['email'] . '</Username><Password>' . $config['password'] . '</Password><IntegratorKey>' . $config['integrator_key'] . '</IntegratorKey></DocuSignCredentials>',
            'Accept' => 'application/json',
            'Content-Type' => 'application/json'
        ];

        $requestUrl = 'https://' . $config['environment']. '.docusign.net/restapi/' . $config['version'] . '/accounts/' . $config['account_id'] . '/';

        $this->client = new Client(array_merge($clientSettings, ['base_uri' => $requestUrl, 'headers' => $headers]));
    }
}
