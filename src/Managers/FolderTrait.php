<?php

namespace Pilyavskiy\Docusign\Managers;

trait FolderTrait
{
    public function getFolders($withInclude = false)
    {
        $response = $this->client->get('folders/?template=' . ($withInclude) ? 'include' : 'only');

        return json_decode($response->getBody()->getContents(), true)['folders'];
    }

    public function getFolderEnvelopes($folderId, $options = null)
    {
        $response = $this->client->get('folders/' . $folderId, ['query' => $options]);

        return json_decode($response->getBody()->getContents(), true);
    }
}
