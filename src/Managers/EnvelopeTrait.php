<?php

namespace Pilyavskiy\Docusign\Managers;

trait EnvelopeTrait
{
    public function getEnvelopes(array $envelopeIds)
    {
        $response = $this->client->put(
            'envelopes/status',
            ['json' => ['envelopeIds' => $envelopeIds], 'query' => ['envelope_ids' => 'request_body']]
        );

        return json_decode($response->getBody()->getContents(), true);
    }

    public function getEnvelope($envelopeId)
    {
        $response = $this->client->get('envelopes/' . $envelopeId);

        return json_decode($response->getBody()->getContents(), true);
    }

    public function getEnvelopePdf($envelopeId)
    {
        $request = $this->client->get('envelopes/' . $envelopeId . '/documents/combined?certificate=true');

        return $request->getBody()->getContents();
    }

    public function getEnvelopeRecipients($envelopeId, $include_tabs = false)
    {
        $response = $this->client->get('envelopes/' . $envelopeId . '/recipients?include_tabs=' . ($include_tabs) ? 'true' : 'false');

        return json_decode($response->getBody()->getContents(), true);
    }

    public function getEnvelopeTabs($envelopeId, $recipientId)
    {
        $response = $this->client->get('envelopes/' . $envelopeId . '/recipients/' . $recipientId . '/tabs');

        return json_decode($response->getBody()->getContents(), true);
    }

    public function createEnvelope($data) {
        $response = $this->client->post('envelopes/', ['json' => $data]);

        return json_decode($response->getBody()->getContents(), true);
    }

    public function updateEnvelope($envelopeId, $data)
    {
        $response = $this->client->put('envelopes/' . $envelopeId, ['json' => $data]);

        return json_decode($response->getBody()->getContents(), true);
    }

    public function updateEnvelopeRecipients($envelopeId, $recipients)
    {
        $response = $this->client->put('envelopes/' . $envelopeId . '/recipients/' , ['json' => $recipients]);

        return json_decode($response->getBody()->getContents(), true);
    }

    public function updateRecipientTabs($envelopeId, $recipientId, $tabs)
    {
        $response = $this->client->put('envelopes/' . $envelopeId . '/recipients/' . $recipientId . '/tabs', ['json' => $tabs]);

        return json_decode($response->getBody()->getContents(), true);
    }

    /**
     * @see https://developers.docusign.com/docs/esign-rest-api/v2/reference/folders/folders/moveenvelopes/
     */
    public function deleteEnvelopes(array $envelopeIds) {
        $response = $this->client->put('folders/recyclebin', ['json' => ['envelopeIds' => $envelopeIds]]);

        return json_decode($response->getBody()->getContents(), true);
    }

    public function getEnvelopeCustomFields($envelopeId)
    {
        $response = $this->client->get('envelopes/' . $envelopeId . '/custom_fields');

        return json_decode($response->getBody()->getContents(), true);
    }

    public function createEnvelopeRecipientView($envelopeId, $data)
    {
        $response = $this->client->post('envelopes/' . $envelopeId . '/views/recipient', ['json' => $data]);

        return json_decode($response->getBody()->getContents(), true);
    }
}
