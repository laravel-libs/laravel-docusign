<?php

return [
    /**
     * The DocuSign Integrator's Key
     */
    'integrator_key' => '',

    /**
     * The Docusign Account Email
     */
    'email' => '',

    /**
     * The Docusign Account Password
     */
    'password' => '',

    /**
     * The DocuSign Account Id
     */
    'account_id' => '',

    /**
     * The version of DocuSign API (Ex: v2, v2.1)
     */
    'version' => 'v2',

    /**
     * The DocuSign Environment (Ex: demo, live)
     */
    'environment' => 'demo',
];
