# DocuSign Rest API Wrapper for Laravel 5.4+ version

This package was developed to request a signature through your Laravel app

### Install package
For download package:
```bash
composer require pilyavskiy/laravel-docusign
```

**This is only required if you are using Laravel 5.4!**
For include into project, please, add DocusignServiceProvider in your `config/app.php` file:
```php
Pilyavskiy\Docusign\DocusignServiceProvider::class
```
